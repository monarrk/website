#!/bin/sh

# temp var
RESULT="<div id=\"imgsh-images\">"

# generate html
for d in "/srv/www/nginx/media"/*; do
	# full path
	P="/media/`basename ${d}`"

	ELEM="img"
	# use <video> for mp4s
	if file --mime-type "${d}" | grep -q mp4$; then
		ELEM="video"
	fi

	# skip txt files
	if file --mime-type "${d}" | grep -q "text/plain"; then continue; fi

	# get a description
	DESCFILE="/srv/www/nginx${P}.txt"
	DESC=""
	[ -e "${DESCFILE}" ] && DESC="<p id=\"${DESCFILE}-desc\" class=\"description\">`cat "${DESCFILE}"`</p>"

	# concat the html
	RESULT="${RESULT}
	<div class=\"img-wrap\">
		<h1>${P}</h1>
		${DESC}
		<a href=\"${P}\" target=\"_blank\"><${ELEM} src=\"${P}\"/></a>
	</div>
	<br/><br/><br/>"
done

# finish div elem
RESULT="${RESULT}
</div>"

# use perl because sed doesn't like html
PATTERN="${RESULT}" perl -p -e 's/{{IMAGES}}/$ENV{PATTERN}/g' /srv/www/nginx/images.template > /srv/www/nginx/images.html

# just to save some time :)
sv restart nginx
